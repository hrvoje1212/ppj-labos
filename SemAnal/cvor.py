#!/usr/bin/python2.7
import sys

class leksJedinka:
	def __init__(self, IDN = "", red = None, ime = ""):
		self.IDN = IDN
		self.red = red
		self.ime = ime
	
	def ispis(self):
		print( self.IDN, " ", self.red, " ", self.ime )
		

class cvor:
    def __init__ (self, jedinka = leksJedinka(), tip = [], ntip = [], ime = "", br_elem = None, l_izraz = None):
        self.jedinka = jedinka
        self.tip = tip
        self.ntip = ntip
        self.ime = ime
        self.br_elem = br_elem
        self.l_izraz = l_izraz
        self.djeca = []

    def dodajDijete(self, dijete):
        self.djeca.append(dijete)

    def __str__(self):
        if self.jedinka.IDN[0] == '<' or self.jedinka.IDN == '$':
            return self.jedinka.IDN
        else:
            return "%s %s %s" % (self.jedinka.IDN, self.jedinka.red, self.jedinka.ime)

    def ispisStabla(self, razmak):
        print "%s%s" % (razmak, self)
        for dijete in self.djeca:
            dijete.ispisStabla(razmak + " ")
