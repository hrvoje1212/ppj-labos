#!/usr/bin/python2.7

class identifikator():
    def __init__ (self, tip = None, ime = "", dekl_def = -1, l_izraz = None, vrijednost = None):
        self.tip = tip              #lista, isto kao u cvoru stabla
        self.ime = ime
        self.vrijednost = vrijednost
        self.dekl_def = dekl_def    # -1 nije ni deklarirano ni definirano
                                    #  0 samo deklarirano
                                    #  1 definirano
        
        self.l_izraz = l_izraz      # 1 - identifikatoru se moze pridruzit vrijednost, 0 ne 


class cvorZnak():
    def __init__ (self, roditelj = None, trenutno_u_djelokrugu = None):
        self.trenutno_u_djelokrugu = trenutno_u_djelokrugu      #lista, isto kao tip u cvoru stabla
        self.lista_identifikatora = []
        self.roditelj = roditelj
        self.djeca = []

    def dodaj_IDN (self, IDN):
		self.lista_identifikatora.append(IDN)

    def dodaj_dijete(self, dijete):
        self.djeca.append(dijete)

