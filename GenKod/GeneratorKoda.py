#!/usr/bin/python2.7
import sys
from cvor import *
from cvorZnak import *


def provjeri_tip(tip1, tip2):
    """ provjerava dal se moze implicitno tip1 ~ tip2 """

    if tip1[0] == tip2[0]:
        return True
    
    if tip1[0] == 'const_int' and tip2[0] == 'int':
        return True

    elif tip1[0] == 'int' and tip2[0] == 'const_int':
        return True

    elif tip1[0] == 'const_char' and tip2[0] == 'char':
        return True

    elif tip1[0] == 'char' and tip2[0] == 'const_char':
        return True

    elif tip1[0] == 'char' and tip2[0] == 'int':
        return True

    elif (tip1[0] == 'niz' and tip1[1] == 'int') and (tip1[0] == 'niz' and tip1[1] == 'const_int'):
        return True

    elif (tip1[0] == 'niz' and tip1[1] == 'char') and (tip1[0] == 'niz' and tip1[1] == 'const_char'):
        return True

    else:
        return False



def nadi_u_tablici_znakova(jedinka, trenutni_cvor_znak):
    """trazi zadanu jedinku u tablici znakova i vraca nadeni IDN ili None"""
    
    for IDN in trenutni_cvor_znak.lista_identifikatora:
        if jedinka.ime == IDN.ime:
            return IDN

    if trenutni_cvor_znak.roditelj == None:
        return None

    return nadi_u_tablici_znakova(jedinka,trenutni_cvor_znak.roditelj)


def provjeri_trenutni_cvor_znakova(jedinka, trenutni_cvor_znak):
    """trazi zadanu jedinku samo u trenutnom cvoru tablice znakova i vraca nadeni IDN ili None"""

    for IDN in trenutni_cvor_znak.lista_identifikatora:
        if jedinka.ime == IDN.ime:
            return IDN

    return None


def nadi_funkciju(ime):
    "trazi zadanu jedinku u tablici znakova i vraca taj cvor"
    
    for IDN in korijen_znakova.lista_identifikatora:
        if IDN.tip[0] == 'fun':
            if ime == IDN.ime:
                return True

    return False



def provjeri(trenutni_cvor, trenutni_cvor_u_tablici_znakova):
    """provjerava svojstva podstabla od trenutnog cvora dolje"""


##################### <primarni_izraz> #####################
    
    if trenutni_cvor.jedinka.IDN == '<primarni_izraz>':

        ##<primarni_izraz> ::= IDN
        if trenutni_cvor.djeca[0].jedinka.IDN == 'IDN':

            ##provjeri je li IDN.ime deklarirano
            IDN = nadi_u_tablici_znakova(trenutni_cvor.djeca[0].jedinka, trenutni_cvor_u_tablici_znakova)

            if IDN == None:
                ispis_greske(trenutni_cvor)

            trenutni_cvor.tip = IDN.tip[:]
            trenutni_cvor.vrijednost = IDN.vrijednost
            trenutni_cvor.l_izraz = IDN.l_izraz
            trenutni_cvor.ime = IDN.ime

            if IDN.tip[0] != 'fun':

                ##varijabla je globalna
                if IDN.ime in globalne_varijable.keys():
                    outputFile.write(("\tLOAD R0, (G_%s)\n" % (IDN.ime)).upper())


                ##varijabla je lokalna
                else:
                    outputFile.write(("\tLOAD R0, (R7 + %s)\n" % (hex(odmak[IDN.ime] + dodatni_odmak )[2:])).upper())

                outputFile.write("\tPUSH R0\n\n")



        ##<primarni_izraz> ::= BROJ
        elif trenutni_cvor.djeca[0].jedinka.IDN == 'BROJ':

            ##provjeri je li vrijednost u rasponu 'int'
            if int(trenutni_cvor.djeca[0].jedinka.ime) < -2**31 or int(trenutni_cvor.djeca[0].jedinka.ime) > 2**31 - 1:
                ispis_greske(trenutni_cvor)
            
            trenutni_cvor.tip = ['int']
            trenutni_cvor.vrijednost = trenutni_cvor.ntip * int(trenutni_cvor.djeca[0].jedinka.ime)
            trenutni_cvor.l_izraz = 0

            if trenutni_cvor_u_tablici_znakova.trenutno_u_djelokrugu != None:

                if trenutni_cvor.vrijednost <= 2**19 -1 and trenutni_cvor.vrijednost >= - 2**19:
                    if trenutni_cvor.vrijednost < 0:
                        outputFile.write(("\tMOVE -%s, R0\n\tPUSH R0\n" % hex(trenutni_cvor.vrijednost)[3:]).upper())
                    else:
                        outputFile.write(("\tMOVE %s, R0\n\tPUSH R0\n" % hex(trenutni_cvor.vrijednost)[2:]).upper())

                else:
                    global broj_konstante
                    globalne_varijable[str(broj_konstante)] = trenutni_cvor.vrijednost
                    outputFile.write(("\tLOAD R0, (G_%s)\n\tPUSH R0\n" % (str(broj_konstante))))
                    broj_konstante += 1


        ##<primarni_izraz> ::= ZNAK
        elif trenutni_cvor.djeca[0].jedinka.IDN == 'ZNAK':

            #provjeri je li znak zadan ispravno
            if trenutni_cvor.djeca[0].jedinka.ime[0] == "\\" and trenutni_cvor.djeca[0].jedinka.ime[1] not in ['n','t','0','\'','\"','\\']:
                ispis_greske(trenutni_cvor)
            
            trenutni_cvor.tip = ['char']
            trenutni_cvor.l_izraz = 0

        ##<primarni_izraz> ::= NIZ_ZNAKOVA
        elif trenutni_cvor.djeca[0].jedinka.IDN == 'NIZ_ZNAKOVA':

            niz = trenutni_cvor.djeca[0].jedinka.ime
            index = niz.find('\\') 

            while index >= 0:
                if niz[index + 1] not in ['n','t','0','\'','\"','\\']:
                    ispis_greske(trenutni_cvor)

                niz = niz[index+1 : ]
                index = niz.find('\\')

            trenutni_cvor.tip = ['niz', 'const_char']
            trenutni_cvor.l_izraz = 0


        ##<primarni_izraz> ::= L_ZAGRADA <izraz> D_ZAGRADA
        elif trenutni_cvor.djeca[0].jedinka.IDN == 'L_ZAGRADA':
            
            provjeri(trenutni_cvor.djeca[1], trenutni_cvor_u_tablici_znakova)    #provjeri(<izraz>)
            trenutni_cvor.tip = trenutni_cvor.djeca[1].tip[:]
            trenutni_cvor.l_izraz = trenutni_cvor.djeca[1].l_izraz

        else:
            ispis_greske(trenutni_cvor)



##################### <postfiks_izraz> #####################
            
    elif trenutni_cvor.jedinka.IDN == '<postfiks_izraz>':

        ##<postfiks_izraz> ::= <primarni_izraz>
        if trenutni_cvor.djeca[0].jedinka.IDN == '<primarni_izraz>':
            
            trenutni_cvor.djeca[0].ntip = trenutni_cvor.ntip

            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<primarni_izraz>)

            trenutni_cvor.tip = trenutni_cvor.djeca[0].tip[:]
            trenutni_cvor.vrijednost = trenutni_cvor.djeca[0].vrijednost
            trenutni_cvor.l_izraz = trenutni_cvor.djeca[0].l_izraz
            trenutni_cvor.ime = trenutni_cvor.djeca[0].ime

        ##<postfiks_izraz> ::= <postfiks_izraz> L_UGL_ZAGRADA <izraz> D_UGL_ZAGRADA
        elif trenutni_cvor.djeca[1].jedinka.IDN == 'L_UGL_ZAGRADA':

            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<postfiks_izraz>)

            if trenutni_cvor.djeca[0].tip[0] != 'niz':
                ispis_greske(trenutni_cvor)

            if trenutni_cvor.djeca[0].tip[1] not in ['int','char','const_int','const_char']:
                ispis_greske(trenutni_cvor)
 
            provjeri(trenutni_cvor.djeca[2], trenutni_cvor_u_tablici_znakova)    #provjeri(<izraz>)

            if provjeri_tip(trenutni_cvor.djeca[2].tip, ['int']) == False:
                ispis_greske(trenutni_cvor)


            trenutni_cvor.tip = [trenutni_cvor.djeca[0].tip[1]]

            if trenutni_cvor.djeca[0].tip[1] in ['const_int','const_char']:
                trenutni_cvor.l_izraz = 0
                
            else:
                trenutni_cvor.l_izraz = 1
            
        
        elif trenutni_cvor.djeca[1].jedinka.IDN == 'L_ZAGRADA':
            
            ##<postfiks_izraz> ::= <postfiks_izraz> L_ZAGRADA D_ZAGRADA
            if trenutni_cvor.djeca[2].jedinka.IDN == 'D_ZAGRADA':
                
                provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<postfiks_izraz>)

                if trenutni_cvor.djeca[0].tip[0] != 'fun':
                    ispis_greske(trenutni_cvor)

                if trenutni_cvor.djeca[0].tip[2] != 'void':
                    ispis_greske(trenutni_cvor)

                trenutni_cvor.tip = [trenutni_cvor.djeca[0].tip[1]]
                trenutni_cvor.l_izraz = 0
                trenutni_cvor.ime = trenutni_cvor.djeca[0].ime

                outputFile.write(("\tCALL F_%s\n" % (trenutni_cvor.djeca[0].ime)).upper())


            ##<postfiks_izraz> ::= <postfiks_izraz> L_ZAGRADA <lista_argumenata> D_ZAGRADA
            elif trenutni_cvor.djeca[2].jedinka.IDN == '<lista_argumenata>':
                
                provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<postfiks_izraz>)
                provjeri(trenutni_cvor.djeca[2], trenutni_cvor_u_tablici_znakova)    #provjeri(<lista_argumenata>)

                if trenutni_cvor.djeca[0].tip[0] != 'fun':
                    ispis_greske(trenutni_cvor)

                if len(trenutni_cvor.djeca[0].tip[2:]) != len(trenutni_cvor.djeca[2].tip):
                    ispis_greske(trenutni_cvor)
                

                for i in range(len(trenutni_cvor.djeca[2].tip)):
                    if provjeri_tip([trenutni_cvor.djeca[2].tip[i]], [trenutni_cvor.djeca[0].tip[i+2]]) == False:
                        ispis_greske(trenutni_cvor)
                        

                trenutni_cvor.tip = [trenutni_cvor.djeca[0].tip[1]]
                trenutni_cvor.l_izraz = 0
                trenutni_cvor.ime = trenutni_cvor.djeca[0].ime


                #stavi predane parametre za funkciju na stog
                for i in range(len(trenutni_cvor.djeca[2].ime)):
                    
                    #parametar je konstanta
                    if trenutni_cvor.djeca[2].ime[i] == "":
                        outputFile.write("\tMOVE %%D %d, R0\n" % (trenutni_cvor.djeca[2].vrijednost[i]))
                        outputFile.flush()


                    #parametar je varijabla
                    else:   
                        #varijabla je globalna
                        if trenutni_cvor.djeca[2].ime[i] in globalne_varijable.keys():
                            outputFile.write(("\tLOAD R0, (G_%s)\n" % (trenutni_cvor.djeca[2].ime)).upper())

                        #varijabla je lokalna
                        else:
                            outputFile.write(("\tLOAD R0, (R7 + %s)\n" % (hex(odmak[trenutni_cvor.djeca[2].ime[i]] + dodatni_odmak )[2:])).upper())


                    outputFile.write("\tPUSH R0\n\n")
                
                outputFile.write(("\tCALL F_%s\n" % (trenutni_cvor.djeca[0].ime)).upper())
                outputFile.write(("\tADD R7, %s, R7\n" % (hex(len(trenutni_cvor.djeca[2].tip) * 4)[2:])).upper())


            else:
                ispis_greske(trenutni_cvor)
                                

        ##<postfiks_izraz> ::= <postfiks_izraz> (OP_INC | OP_DEC)
        elif trenutni_cvor.djeca[1].jedinka.IDN in ['OP_INC','OP_DEC']:
            
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<postfiks_izraz>)
            
            if trenutni_cvor.djeca[0].l_izraz != 1:
                ispis_greske(trenutni_cvor)
            if provjeri_tip(trenutni_cvor.djeca[0].tip, ['int']) == False:
                ispis_greske(trenutni_cvor)

            trenutni_cvor.tip = ['int']
            trenutni_cvor.l_izraz = 0

        else:
            ispis_greske(trenutni_cvor)


##################### <lista_argumenata> #####################
    
    elif trenutni_cvor.jedinka.IDN == '<lista_argumenata>':

        ##<lista_argumenata> ::= <izraz_pridruzivanja>
        if trenutni_cvor.djeca[0].jedinka.IDN == '<izraz_pridruzivanja>':
            
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<izraz_pridruzivanja>)
            trenutni_cvor.tip = trenutni_cvor.djeca[0].tip[:]
            trenutni_cvor.vrijednost = [trenutni_cvor.djeca[0].vrijednost]
            trenutni_cvor.ime = [trenutni_cvor.djeca[0].ime]

        ##<lista_argumenata> ::= <lista_argumenata> ZAREZ <izraz_pridruzivanja>
        elif trenutni_cvor.djeca[0].jedinka.IDN == '<lista_argumenata>':
            
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<lista_argumenata>)
            provjeri(trenutni_cvor.djeca[2], trenutni_cvor_u_tablici_znakova)    #provjeri(<izraz_pridruzivanja>)

            trenutni_cvor.tip = trenutni_cvor.djeca[0].tip + trenutni_cvor.djeca[2].tip
            trenutni_cvor.vrijednost = trenutni_cvor.djeca[0].vrijednost + [trenutni_cvor.djeca[2].vrijednost]
            trenutni_cvor.ime = trenutni_cvor.djeca[0].ime + [trenutni_cvor.djeca[2].ime]


        else:
            ispis_greske(trenutni_cvor)
            

##################### <unarni_izraz> #####################
            
    elif trenutni_cvor.jedinka.IDN == '<unarni_izraz>':

        ##<unarni_izraz> ::= <postfiks_izraz>
        if trenutni_cvor.djeca[0].jedinka.IDN == '<postfiks_izraz>':
            
            if trenutni_cvor.ntip == []:
                trenutni_cvor.djeca[0].ntip = 1
            else:
                trenutni_cvor.djeca[0].ntip = trenutni_cvor.ntip

            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<postfiks_izraz>)

            trenutni_cvor.tip = trenutni_cvor.djeca[0].tip[:]
            trenutni_cvor.vrijednost = trenutni_cvor.djeca[0].vrijednost
            trenutni_cvor.l_izraz = trenutni_cvor.djeca[0].l_izraz
            trenutni_cvor.ime = trenutni_cvor.djeca[0].ime


        ##<unarni_izraz> ::= (OP_INC | OP_DEC) <unarni_izraz>
        elif trenutni_cvor.djeca[0].jedinka.IDN in ['OP_INC', 'OP_DEC']:
            
            trenutni_cvor.djeca[0].ntip = 1

            provjeri(trenutni_cvor.djeca[1], trenutni_cvor_u_tablici_znakova)    #provjeri(<unarni_izraz>)

            if trenutni_cvor.djeca[1].l_izraz != 1:
                ispis_greske(trenutni_cvor)

            if provjeri_tip(trenutni_cvor.djeca[1].tip, ['int']) == False:
                ispis_greske(trenutni_cvor)
                
            trenutni_cvor.tip = ['int']
            trenutni_cvor.l_izraz = 0


        ##<unarni_izraz> ::= <unarni_operator> <cast_izraz>
        elif trenutni_cvor.djeca[0].jedinka.IDN == '<unarni_operator>':
            
            unarni_operator = trenutni_cvor.djeca[0]

            if unarni_operator.djeca[0].jedinka.IDN == 'MINUS':
                trenutni_cvor.djeca[1].ntip = -1

            else:
                trenutni_cvor.djeca[1].ntip = 1
                

            provjeri(trenutni_cvor.djeca[1], trenutni_cvor_u_tablici_znakova)    #provjeri(<cast_izraz>)


            if provjeri_tip(trenutni_cvor.djeca[1].tip, ['int']) == False:
                ispis_greske(trenutni_cvor)


            if unarni_operator.djeca[0].jedinka.IDN == 'MINUS':
               trenutni_cvor.vrijednost = -trenutni_cvor.djeca[1].vrijednost

            else:
                trenutni_cvor.vrijednost = trenutni_cvor.djeca[1].vrijednost
            
            
            trenutni_cvor.tip = ['int']
            trenutni_cvor.l_izraz = 0


        else:
            ispis_greske(trenutni_cvor)



##################### <cast_izraz>  #####################

    elif trenutni_cvor.jedinka.IDN == '<cast_izraz>':

        ##<cast_izraz> ::= <unarni_izraz>
        if trenutni_cvor.djeca[0].jedinka.IDN == '<unarni_izraz>':
            
            trenutni_cvor.djeca[0].ntip = trenutni_cvor.ntip

            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri('<unarni_izraz>')

            trenutni_cvor.tip = trenutni_cvor.djeca[0].tip[:]
            trenutni_cvor.vrijednost = trenutni_cvor.djeca[0].vrijednost
            trenutni_cvor.l_izraz = trenutni_cvor.djeca[0].l_izraz
            trenutni_cvor.ime = trenutni_cvor.djeca[0].ime


        ##<cast_izraz> ::= L_ZAGRADA <ime_tipa> D_ZAGRADA <cast_izraz>
        elif trenutni_cvor.djeca[0].jedinka.IDN == 'L_ZAGRADA':
            
            provjeri(trenutni_cvor.djeca[1], trenutni_cvor_u_tablici_znakova)    #provjeri(<ime_tipa>)
            provjeri(trenutni_cvor.djeca[3], trenutni_cvor_u_tablici_znakova)    #provjeri('<cast_izraz>')

            if trenutni_cvor.djeca[1].tip[0] not in ['int', 'char', 'const_int', 'const_char']:
                ispis_greske(trenutni_cvor)

            if trenutni_cvor.djeca[3].tip[0] not in ['int', 'char', 'const_int', 'const_char']:
                ispis_greske(trenutni_cvor)
            
            trenutni_cvor.tip = trenutni_cvor.djeca[1].tip[:]
            trenutni_cvor.l_izraz = 0
            trenutni_cvor.vrijednost = trenutni_cvor.djeca[3].vrijednost

        else:
            ispis_greske(trenutni_cvor)


            
##################### <ime_tipa> #####################
            
    elif trenutni_cvor.jedinka.IDN == '<ime_tipa>':

        ##<ime_tipa> ::= <specifikator_tipa>
        if trenutni_cvor.djeca[0].jedinka.IDN == '<specifikator_tipa>':
            
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<specifikator_tipa>)
            trenutni_cvor.tip = trenutni_cvor.djeca[0].tip[:]


        ##<ime_tipa> ::= KR_CONST <specifikator_tipa>
        elif trenutni_cvor.djeca[0].jedinka.IDN == 'KR_CONST':
            
            provjeri(trenutni_cvor.djeca[1], trenutni_cvor_u_tablici_znakova)    #provjeri(<specifikator_tipa>)

            if 'void' in trenutni_cvor.djeca[1].tip:
                ispis_greske(trenutni_cvor)

            trenutni_cvor.tip = ['const_' + trenutni_cvor.djeca[1].tip[0]]

        else:
            ispis_greske(trenutni_cvor)
            
            

##################### <specifikator_tipa> #####################
            
    elif trenutni_cvor.jedinka.IDN == '<specifikator_tipa>':

        if trenutni_cvor.djeca[0].jedinka.IDN == 'KR_VOID':
            trenutni_cvor.tip = ['void']

        elif trenutni_cvor.djeca[0].jedinka.IDN == 'KR_CHAR':
            trenutni_cvor.tip = ['char']

        elif trenutni_cvor.djeca[0].jedinka.IDN == 'KR_INT':
            trenutni_cvor.tip = ['int']

        else:
            ispis_greske(trenutni_cvor)


### <multiplikativni_izraz>,'<aditivni_izraz>','<odnosni_izraz>','<jednakosni_izraz>','<bin_i_izraz>','<bin_xili_izraz>','<bin_ili_izraz>','<log_i_izraz>','<log_ili_izraz>' ###
            
    elif trenutni_cvor.jedinka.IDN in ['<multiplikativni_izraz>','<aditivni_izraz>','<odnosni_izraz>','<jednakosni_izraz>','<bin_i_izraz>','<bin_xili_izraz>','<bin_ili_izraz>','<log_i_izraz>','<log_ili_izraz>']:
        if len(trenutni_cvor.djeca) == 1:
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)

            trenutni_cvor.tip = trenutni_cvor.djeca[0].tip[:]
            trenutni_cvor.vrijednost = trenutni_cvor.djeca[0].vrijednost
            trenutni_cvor.l_izraz = trenutni_cvor.djeca[0].l_izraz
            trenutni_cvor.ime = trenutni_cvor.djeca[0].ime


        elif trenutni_cvor.djeca[1].jedinka.IDN in ['OP_PUTA', 'OP_DIJELI', 'OP_MOD','PLUS','MINUS','OP_LT','OP_GT','OP_LTE','OP_GTE','OP_EQ','OP_NEQ','OP_BIN_I','OP_BIN_XILI','OP_BIN_ILI','OP_I','OP_ILI']:


            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)   #provjeri (<aditivni_izraz>)

            if provjeri_tip(trenutni_cvor.djeca[0].tip, ['int']) == False:
                ispis_greske(trenutni_cvor)
                        
            provjeri(trenutni_cvor.djeca[2], trenutni_cvor_u_tablici_znakova)   #provjeri (<multiplikativni_izraz>)
                
            if provjeri_tip(trenutni_cvor.djeca[2].tip, ['int']) == False:
                ispis_greske(trenutni_cvor)


            if trenutni_cvor.djeca[1].jedinka.IDN == 'PLUS':
                outputFile.write("\tPOP R1\n\tPOP R0\n\tADD R0, R1, R0\n\tPUSH R0\n\n")
                
            elif trenutni_cvor.djeca[1].jedinka.IDN == 'MINUS':
                outputFile.write("\tPOP R1\n\tPOP R0\n\tSUB R0, R1, R0\n\tPUSH R0\n\n")

            elif trenutni_cvor.djeca[1].jedinka.IDN == 'OP_BIN_I':
                outputFile.write("\tPOP R1\n\tPOP R0\n\tAND R0, R1, R0\n\tPUSH R0\n\n")

            elif trenutni_cvor.djeca[1].jedinka.IDN == 'OP_BIN_XILI':
                outputFile.write("\tPOP R1\n\tPOP R0\n\tXOR R0, R1, R0\n\tPUSH R0\n\n")

            elif trenutni_cvor.djeca[1].jedinka.IDN == 'OP_BIN_ILI':
                outputFile.write("\tPOP R1\n\tPOP R0\n\tOR R0, R1, R0\n\tPUSH R0\n\n")

            else:
                ispis_greske()
                


            trenutni_cvor.tip = ['int']
            trenutni_cvor.l_izraz = 0


        else:
            ispis_greske(trenutni_cvor)
            


##################### <izraz_pridruzivanja> #####################
 
    elif trenutni_cvor.jedinka.IDN == '<izraz_pridruzivanja>':

        ##<izraz_pridruzivanja> ::= <log_ili_izraz>
        if trenutni_cvor.djeca[0].jedinka.IDN == '<log_ili_izraz>':
            
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<log_ili_izraz>)

            trenutni_cvor.tip = trenutni_cvor.djeca[0].tip[:]
            trenutni_cvor.vrijednost = trenutni_cvor.djeca[0].vrijednost
            trenutni_cvor.l_izraz = trenutni_cvor.djeca[0].l_izraz
            trenutni_cvor.ime = trenutni_cvor.djeca[0].ime


        ##<izraz_pridruzivanja> ::= <postfiks_izraz> OP_PRIDRUZI <izraz_pridruzivanja>
        elif trenutni_cvor.djeca[0].jedinka.IDN == '<postfiks_izraz>':
            
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<postfiks_izraz>)

            if trenutni_cvor.djeca[0].l_izraz != 1:
                ispis_greske(trenutni_cvor)
            
            provjeri(trenutni_cvor.djeca[2], trenutni_cvor_u_tablici_znakova)    #provjeri(<izraz_pridruzivanja>)

            if provjeri_tip(trenutni_cvor.djeca[2].tip, trenutni_cvor.djeca[0].tip) == False:
                ispis_greske(trenutni_cvor)

            trenutni_cvor.tip = trenutni_cvor.djeca[0].tip[:]
            trenutni_cvor.l_izraz = 0

        else:
            ispis_greske(trenutni_cvor)


        
##################### <izraz> #####################
            
    elif trenutni_cvor.jedinka.IDN == '<izraz>':

        ##<izraz> ::= <izraz_pridruzivanja>
        if trenutni_cvor.djeca[0].jedinka.IDN == '<izraz_pridruzivanja>':
            
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<izraz_pridruzivanja>)

            trenutni_cvor.tip = trenutni_cvor.djeca[0].tip[:]
            trenutni_cvor.vrijednost = trenutni_cvor.djeca[0].vrijednost
            trenutni_cvor.l_izraz = trenutni_cvor.djeca[0].l_izraz
            trenutni_cvor.ime = trenutni_cvor.djeca[0].ime


        ##<izraz> ::= <izraz> ZAREZ <izraz_pridruzivanja>
        elif trenutni_cvor.djeca[0].jedinka.IDN == '<izraz>':
            
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<izraz>)
            provjeri(trenutni_cvor.djeca[2], trenutni_cvor_u_tablici_znakova)    #provjeri(<izraz_pridruzivanja>)

            trenutni_cvor.tip = trenutni_cvor.djeca[2].tip[:]
            trenutni_cvor.l_izraz = 0

        else:
            ispis_greske(trenutni_cvor)



##################### <slozena_naredba> #####################
            
    elif trenutni_cvor.jedinka.IDN == '<slozena_naredba>':

        ##<slozena_naredba> ::= L_VIT_ZAGRADA <lista_naredbi> D_VIT_ZAGRADA
        if trenutni_cvor.djeca[1].jedinka.IDN == '<lista_naredbi>':
            
            provjeri(trenutni_cvor.djeca[1], trenutni_cvor_u_tablici_znakova)    #provjeri(<lista_naredbi>)


        ##<slozena_naredba> ::= L_VIT_ZAGRADA <lista_deklaracija> <lista_naredbi> D_VIT_ZAGRADA
        elif trenutni_cvor.djeca[1].jedinka.IDN == '<lista_deklaracija>':
            
            provjeri(trenutni_cvor.djeca[1], trenutni_cvor_u_tablici_znakova)    #provjeri(<lista_deklaracija>)

            #ostavaljnje mjesta za lok. varijable na stogu
            if hex(trenutni_cvor.djeca[1].broj_varijabli * 4)[2] in ['a','b','c','d','e','f']:
                outputFile.write("\tSUB R7, 0%s, R7\n" % (hex(trenutni_cvor.djeca[1].broj_varijabli * 4))[2:].upper())

            else:
                outputFile.write("\tSUB R7, %s, R7\n" % (hex(trenutni_cvor.djeca[1].broj_varijabli * 4))[2:].upper())


            global dodatni_odmak
            dodatni_odmak += (trenutni_cvor.djeca[1].broj_varijabli * 4)


            provjeri(trenutni_cvor.djeca[2], trenutni_cvor_u_tablici_znakova)    #provjeri(<lista_naredbi>)


            #skidanje lok. varijabli sa stoga
            if hex(trenutni_cvor.djeca[1].broj_varijabli * 4)[2] in ['a','b','c','d','e','f']:
                outputFile.write("\tADD R7, 0%s, R7\n" % (hex(trenutni_cvor.djeca[1].broj_varijabli * 4))[2:].upper())

            else:
                outputFile.write("\tADD R7, %s, R7\n" % (hex(trenutni_cvor.djeca[1].broj_varijabli * 4))[2:].upper())

            dodatni_odmak -= (trenutni_cvor.djeca[1].broj_varijabli * 4)

            

        else:
            ispis_greske(trenutni_cvor)


##################### <lista_naredbi> #####################
            
    elif trenutni_cvor.jedinka.IDN == '<lista_naredbi>':

        ##<lista_naredbi> ::= <naredba>
        if trenutni_cvor.djeca[0].jedinka.IDN == '<naredba>':
            
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<naredba>)


        ##<lista_naredbi> ::= <lista_naredbi> <naredba>
        elif trenutni_cvor.djeca[0].jedinka.IDN == '<lista_naredbi>':
            
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<lista_naredbi>)
            provjeri(trenutni_cvor.djeca[1], trenutni_cvor_u_tablici_znakova)    #provjeri(<naredba>)


        else:
            ispis_greske(trenutni_cvor)


            
##################### <naredba> #####################
            
    elif trenutni_cvor.jedinka.IDN == '<naredba>':
        
        ##<naredba> ::= <slozena_naredba>
        if trenutni_cvor.djeca[0].jedinka.IDN == '<slozena_naredba>':
            
            #stvori novi cvor u tablici znakova
            novi_cvor_znak = cvorZnak()
            novi_cvor_znak.roditelj = trenutni_cvor_u_tablici_znakova
            trenutni_cvor_u_tablici_znakova.dodaj_dijete(novi_cvor_znak)

            #oznaci da je blok u djelokrugu iste funkcije
            novi_cvor_znak.trenutno_u_djelokrugu = trenutni_cvor_u_tablici_znakova.trenutno_u_djelokrugu[:]

            provjeri(trenutni_cvor.djeca[0], novi_cvor_znak)    #provjeri(<slozena_naredba>)

            #vrati se u prethodni djelokrug deklaracija
            trenutni_cvor_u_tablici_znakova = trenutni_cvor_u_tablici_znakova.roditelj


        ##<naredba> ::= <izraz_naredba> | <naredba_grananja> | <naredba_petlje> | <naredba_skoka>
        elif trenutni_cvor.djeca[0].jedinka.IDN in ['<izraz_naredba>','<naredba_grananja>','<naredba_petlje>','<naredba_skoka>']:
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)

        else:
            ispis_greske(trenutni_cvor)


##################### <izraz_naredba> #####################
            
    elif trenutni_cvor.jedinka.IDN == '<izraz_naredba>':

        ##<izraz_naredba> ::= TOCKAZAREZ
        if trenutni_cvor.djeca[0].jedinka.IDN == 'TOCKAZAREZ':
            trenutni_cvor.tip = ['int']


        ##<izraz_naredba> ::= <izraz> TOCKAZAREZ
        elif trenutni_cvor.djeca[0].jedinka.IDN == '<izraz>':
            
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<izraz>)
            trenutni_cvor.tip = trenutni_cvor.djeca[0].tip[:]


        else:
            ispis_greske(trenutni_cvor)


##################### <naredba_grananja> #####################
            
    elif trenutni_cvor.jedinka.IDN == '<naredba_grananja>':

        ##<naredba_grananja> ::= KR_IF L_ZAGRADA <izraz> D_ZAGRADA <naredba>
        if len(trenutni_cvor.djeca) == 5:
            
            provjeri(trenutni_cvor.djeca[2], trenutni_cvor_u_tablici_znakova)    #provjeri(<izraz>)

            if provjeri_tip(trenutni_cvor.djeca[2].tip, ['int']) == False:
                ispis_greske(trenutni_cvor)

            provjeri(trenutni_cvor.djeca[4], trenutni_cvor_u_tablici_znakova)    #provjeri(<naredba>)


        ##<naredba_grananja> ::= KR_IF L_ZAGRADA <izraz> D_ZAGRADA <naredba> KR_ELSE <naredba>
        elif len(trenutni_cvor.djeca) == 7:
            
            provjeri(trenutni_cvor.djeca[2], trenutni_cvor_u_tablici_znakova)    #provjeri(<izraz>)

            if provjeri_tip(trenutni_cvor.djeca[2].tip, ['int']) == False:
                ispis_greske(trenutni_cvor)
    
            provjeri(trenutni_cvor.djeca[4], trenutni_cvor_u_tablici_znakova)    #provjeri(<naredba>)
            provjeri(trenutni_cvor.djeca[6], trenutni_cvor_u_tablici_znakova)    #provjeri(<naredba>)


        else:
            ispis_greske(trenutni_znak)
            


##################### <naredba_petlje> #####################
            
    elif trenutni_cvor.jedinka.IDN == '<naredba_petlje>':

        ##<naredba_petlje> ::= KR_WHILE L_ZAGRADA <izraz> D_ZAGRADA <naredba>
        if trenutni_cvor.djeca[0].jedinka.IDN == 'KR_WHILE':
            
            provjeri(trenutni_cvor.djeca[2], trenutni_cvor_u_tablici_znakova)    #provjeri(<izraz>)

            if provjeri_tip(trenutni_cvor.djeca[2].tip, ['int']) == False:
                ispis_greske(trenutni_cvor)

            global u_petlji
            u_petlji += 1
            provjeri(trenutni_cvor.djeca[4], trenutni_cvor_u_tablici_znakova)    #provjeri(<naredba>)
            u_petlji -= 1


        ##<naredba_petlje> ::= KR_FOR L_ZAGRADA <izraz_naredba> <izraz_naredba> D_ZAGRADA <naredba>
        elif trenutni_cvor.djeca[4].jedinka.IDN == 'D_ZAGRADA':
            
            provjeri(trenutni_cvor.djeca[2], trenutni_cvor_u_tablici_znakova)    #provjeri(<izraz_naredba>)
            provjeri(trenutni_cvor.djeca[3], trenutni_cvor_u_tablici_znakova)    #provjeri(<izraz_naredba>)

            if provjeri_tip(trenutni_cvor.djeca[3].tip, ['int']) == False:
                ispis_greske(trenutni_cvor)

            global u_petlji
            u_petlji += 1
            provjeri(trenutni_cvor.djeca[5], trenutni_cvor_u_tablici_znakova)    #provjeri(<naredba>)
            u_petlji -= 1


        ##<naredba_petlje> ::= KR_FOR L_ZAGRADA <izraz_naredba> <izraz_naredba> <izraz> D_ZAGRADA <naredba>
        elif trenutni_cvor.djeca[4].jedinka.IDN == '<izraz>':
            
            provjeri(trenutni_cvor.djeca[2], trenutni_cvor_u_tablici_znakova)    #provjeri(<izraz_naredba>)
            provjeri(trenutni_cvor.djeca[3], trenutni_cvor_u_tablici_znakova)    #provjeri(<izraz_naredba>)

            if provjeri_tip(trenutni_cvor.djeca[3].tip, ['int']) == False:
                ispis_greske(trenutni_cvor)

            provjeri(trenutni_cvor.djeca[4], trenutni_cvor_u_tablici_znakova)    #provjeri(<izraz>)

            global u_petlji
            u_petlji += 1
            provjeri(trenutni_cvor.djeca[6], trenutni_cvor_u_tablici_znakova)    #provjeri(<naredba>)
            u_petlji -= 1


        else:
            ispis_greske(trenutni_cvor)
        
                     
##################### <naredba_skoka> #####################
            
    elif trenutni_cvor.jedinka.IDN == '<naredba_skoka>':

        ##<naredba_skoka> ::= (KR_CONTINUE | KR_BREAK) TOCKAZAREZ
        if trenutni_cvor.djeca[0].jedinka.IDN  in ['KR_CONTINUE', 'KR_BREAK']:
            
            ##provjeri dal se naredba nalazi u petlji
            global u_petlji
            if u_petlji <= 0:
                ispis_greske(trenutni_cvor)
            

        ##<naredba_skoka> ::= KR_RETURN TOCKAZAREZ
        elif trenutni_cvor.djeca[1].jedinka.IDN == 'TOCKAZAREZ':
            
            ##provjeriti dal se return nalazi u funkciji tipa VOID
            if trenutni_cvor_u_tablici_znakova.trenutno_u_djelokrugu[1] != 'void':
                ispis_greske(trenutni_cvor)


        ##<naredba_skoka> ::= KR_RETURN <izraz> TOCKAZAREZ
        elif trenutni_cvor.djeca[1].jedinka.IDN == '<izraz>':
            provjeri(trenutni_cvor.djeca[1], trenutni_cvor_u_tablici_znakova)    #provjeri(<izraz>)
            
            ##naredba se nalazi unutar funkcija(params -> pov)
            if trenutni_cvor_u_tablici_znakova.trenutno_u_djelokrugu[1] == 'void':
                ispis_greske(trenutni_cvor)
            
            ##vrijedi <izraz>.tip ~ pov
            if provjeri_tip(trenutni_cvor.djeca[1].tip, [trenutni_cvor_u_tablici_znakova.trenutno_u_djelokrugu[1]]) == False:
                ispis_greske(trenutni_cvor)
            
            if not nadi_funkciju(trenutni_cvor.djeca[1].ime):
                outputFile.write("\tPOP R6\n")

        else:
            ispis_greske(trenutni_cvor)



##################### <prijevodna_jedinica> #####################
            
    elif trenutni_cvor.jedinka.IDN == '<prijevodna_jedinica>':

        ##<prijevodna_jedinica> ::= <vanjska_deklaracija>
        if trenutni_cvor.djeca[0].jedinka.IDN == '<vanjska_deklaracija>':
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<vanjska_deklaracija>)

            trenutni_cvor.broj_varijabli = trenutni_cvor.djeca[0].broj_varijabli

        #<prijevodna_jedinica> ::= <prijevodna_jedinica> <vanjska_deklaracija>
        elif trenutni_cvor.djeca[0].jedinka.IDN == '<prijevodna_jedinica>':
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<prijevodna_jedinica>)
            provjeri(trenutni_cvor.djeca[1], trenutni_cvor_u_tablici_znakova)    #provjeri(<vanjska_deklaracija>)

            trenutni_cvor.broj_varijabli = trenutni_cvor.djeca[0].broj_varijabli + trenutni_cvor.djeca[1].broj_varijabli


        else:
            ispis_greske(trenutni_cvor)



##################### <vanjska_deklaracija> #####################
            
    elif trenutni_cvor.jedinka.IDN == '<vanjska_deklaracija>':

        if trenutni_cvor.djeca[0].jedinka.IDN == '<deklaracija>':
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)

            trenutni_cvor.broj_varijabli = trenutni_cvor.djeca[0].broj_varijabli
           

        elif trenutni_cvor.djeca[0].jedinka.IDN == '<definicija_funkcije>':
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)
       

        else:
            ispis_greske(trenutni_cvor)



##################### <definicija_funkcije> #####################
            
    elif trenutni_cvor.jedinka.IDN == '<definicija_funkcije>':

        ##<definicija_funkcije> ::= <ime_tipa> IDN L_ZAGRADA KR_VOID D_ZAGRADA <slozena_naredba>
        if trenutni_cvor.djeca[3].jedinka.IDN == 'KR_VOID':
            
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<ime_tipa>)

            if trenutni_cvor.djeca[0].tip[0] in ['const_int', 'const_char']:
                ispis_greske(trenutni_cvor)

    
            IDN = provjeri_trenutni_cvor_znakova(trenutni_cvor.djeca[1].jedinka, korijen_znakova) 

            ##funkcija ne smije bit vec definirana
            if IDN != None:
                if IDN.dekl_def == 1:
                    ispis_greske(trenutni_cvor)

                ##ako je samo deklarirana
                if IDN.dekl_def == 0:

                    ##funkcija mora bit deklarirana VOID
                    if 'void' not in IDN.tip[2:]:
                        ispis_greske(trenutni_cvor)

                    ##funkcija mora imati isti tip kao <ime_tipa>.tip
                    if IDN.tip[1] != trenutni_cvor.djeca[0].tip[0]:
                        ispis_greske(trenutni_cvor)

                    IDN.dekl_def = 1
                        
            else:
                ##dodaj definiciju funkcije u trenutni cvor u tablici znakova
                IDN = identifikator(['fun', trenutni_cvor.djeca[0].tip[0],'void'], trenutni_cvor.djeca[1].jedinka.ime, 1, 0)
                trenutni_cvor_u_tablici_znakova.dodaj_IDN(IDN)
                

            ##stvori novi cvor u tablici znakova
            novi_cvor_znak = cvorZnak()
            novi_cvor_znak.roditelj = trenutni_cvor_u_tablici_znakova
            trenutni_cvor_u_tablici_znakova.dodaj_dijete(novi_cvor_znak)
                
            ##oznaci u djelokrugu koje funkcije se nalazis da se zna u <naredba_skoka> (radi RETURN)
            novi_cvor_znak.trenutno_u_djelokrugu = ['fun', trenutni_cvor.djeca[0].tip[0], 'void']


            outputFile.write(("F_%s" % (trenutni_cvor.djeca[1].jedinka.ime)).upper())


            provjeri(trenutni_cvor.djeca[5], novi_cvor_znak)    #provjeri(<slozena_naredba>)

            ##vrati se na roditelja u tablici znakova
            trenutni_cvor_u_tablici_znakova = trenutni_cvor_u_tablici_znakova.roditelj

            outputFile.write("\tRET\n\n")


        ##<definicija_funkcije> ::= <ime_tipa> IDN L_ZAGRADA <lista_parametara> D_ZAGRADA <slozena_naredba>
        elif trenutni_cvor.djeca[3].jedinka.IDN == '<lista_parametara>':
            
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<ime_tipa>)

            if trenutni_cvor.djeca[0].tip[0] in ['const_int', 'const_char']:
                ispis_greske(trenutni_cvor)

            IDN = provjeri_trenutni_cvor_znakova(trenutni_cvor.djeca[1].jedinka, korijen_znakova) 

            ##funkcija ne smije bit vec definirana
            if IDN != None:

                #funkcija je vec definirana
                if IDN.dekl_def == 1: 
                    ispis_greske(trenutni_cvor)                 


            provjeri(trenutni_cvor.djeca[3], trenutni_cvor_u_tablici_znakova)    #provjeri(<lista_parametara>)

            trenutni_odmak = 8
            #zapamti odmak za svaki parametar na stogu
            for ime in reversed(trenutni_cvor.djeca[3].ime):
                odmak[ime] = trenutni_odmak
                trenutni_odmak += 4
                

            if IDN != None:

                #funkcija je vec deklarirana
                if IDN.dekl_def == 0:
                    if IDN.tip[1] != trenutni_cvor.djeca[0].tip[0]: 
                        ispis_greske(trenutni_cvor)

                    if IDN.tip[2:] != trenutni_cvor.djeca[3].tip:
                        ispis_greske(trenutni_cvor)

                    IDN.dekl_def = 1

                    ##oznaci u djelokrugu koje funkcije se nalazis da se zna u <naredba_skoka> (radi RETURN)
                    novi_cvor_znak = cvorZnak()
                    novi_cvor_znak.trenutno_u_djelokrugu = IDN.tip[:]

            else:
                ##dodaj definiciju funkcije u trenutni cvor u tablici znakova
                tip_funkcije = ['fun', trenutni_cvor.djeca[0].tip[0]]   #zabiljezi tip funkcije
                tip_funkcije.extend(trenutni_cvor.djeca[3].tip)         #u tip dodaj listu parametara

                IDN = identifikator(tip_funkcije, trenutni_cvor.djeca[1].jedinka.ime, 1, 0)
                trenutni_cvor_u_tablici_znakova.dodaj_IDN(IDN)

                novi_cvor_znak = cvorZnak()
                novi_cvor_znak.trenutno_u_djelokrugu = tip_funkcije[:]
                    

            ## stvori novi cvor u tablici znakova
            
            novi_cvor_znak.roditelj = trenutni_cvor_u_tablici_znakova
            trenutni_cvor_u_tablici_znakova.dodaj_dijete(novi_cvor_znak)

            ## u LOKALNI djelokrug (novo stvoreni cvor) ugradi parametre funkcije

            #ako je tip niz, stavi ga u listu npr ['niz', 'int']
            tipovi = trenutni_cvor.djeca[3].tip[:]      #kopiraj tip da ga ne izmijenis

            for i in range(tipovi.count('niz')):
                index = tipovi.index('niz')
                tipovi[index] = [tipovi[index], tipovi[index+1]]
                del tipovi[index+1]


            for i in range(len(tipovi)):

                #radi nizova
                if type(tipovi[i]) == list:
                     IDN = identifikator(tipovi[i], trenutni_cvor.djeca[3].ime[i],1,1)

                #za ostale tipove varijabli
                else:
                    IDN = identifikator([tipovi[i]], trenutni_cvor.djeca[3].ime[i],1,1)

                novi_cvor_znak.dodaj_IDN(IDN)
                

            outputFile.write(("F_%s" % (trenutni_cvor.djeca[1].jedinka.ime)).upper())
                   
            provjeri(trenutni_cvor.djeca[5], novi_cvor_znak)    #provjeri(<slozena_naredba>)

            ##vrati se na roditelja u tablici znakova
            trenutni_cvor_u_tablici_znakova = trenutni_cvor_u_tablici_znakova.roditelj

            outputFile.write("\tRET\n\n")


        else:
            ispis_greske(trenutni_cvor)


##################### <lista_parametara> #####################
            
    elif trenutni_cvor.jedinka.IDN == '<lista_parametara>':

        ##<lista_parametara> ::= <deklaracija_parametra>
        if trenutni_cvor.djeca[0].jedinka.IDN == '<deklaracija_parametra>':
            
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri('<deklaracija_parametra>)
            trenutni_cvor.tip = trenutni_cvor.djeca[0].tip[:]
            trenutni_cvor.ime = trenutni_cvor.djeca[0].ime[:]

            
        ##<lista_parametara> ::= <lista_parametara> ZAREZ <deklaracija_parametra>
        elif trenutni_cvor.djeca[0].jedinka.IDN == '<lista_parametara>':
            
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<lista_parametara>)
            provjeri(trenutni_cvor.djeca[2], trenutni_cvor_u_tablici_znakova)    #provjeri(<deklaracija_parametra>)

            #provjeri da ne postoje 2 parametra s istim imenom
            if trenutni_cvor.djeca[2].ime[0] in trenutni_cvor.djeca[0].ime:
                ispis_greske(trenutni_cvor)

            trenutni_cvor.tip = trenutni_cvor.djeca[0].tip + trenutni_cvor.djeca[2].tip     #append u listu tipova
            trenutni_cvor.ime = trenutni_cvor.djeca[0].ime + trenutni_cvor.djeca[2].ime     #append u listu imena
            

        else:
            ispis_greske(trenutni_cvor)



##################### <deklaracija_parametra> ##################### 
    
    elif trenutni_cvor.jedinka.IDN == '<deklaracija_parametra>':

        ##<deklaracija_parametra> ::= <ime_tipa> IDN
        if len(trenutni_cvor.djeca) == 2:
            
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<ime_tipa>)
            if 'void' in trenutni_cvor.djeca[0].tip:
                ispis_greske(trenutni_cvor)

            trenutni_cvor.tip = trenutni_cvor.djeca[0].tip[:]
            trenutni_cvor.ime = [trenutni_cvor.djeca[1].jedinka.ime]


        ##<deklaracija_parametra> ::= <ime_tipa> IDN L_UGL_ZAGRADA D_UGL_ZAGRADA
        elif len(trenutni_cvor.djeca) == 4:
            
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<ime_tipa>)
            if 'void' in trenutni_cvor.djeca[0].tip:
                ispis_greske(trenutni_cvor)

            trenutni_cvor.tip = ['niz', trenutni_cvor.djeca[0].tip[0]]
            trenutni_cvor.ime = [trenutni_cvor.djeca[1].jedinka.ime]


        else:
            ispis_greske(trenutni_cvor)



##################### <lista_deklaracija> ##################### 
    
    elif trenutni_cvor.jedinka.IDN == '<lista_deklaracija>':

        ##<lista_deklaracija> ::= <deklaracija>
        if trenutni_cvor.djeca[0].jedinka.IDN == '<deklaracija>':
            
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<deklaracija>)

            trenutni_cvor.broj_varijabli = trenutni_cvor.djeca[0].broj_varijabli 


        ##<lista_deklaracija> ::= <lista_deklaracija> <deklaracija>
        elif trenutni_cvor.djeca[0].jedinka.IDN == '<lista_deklaracija>':
            
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(lista_deklaracija>)
            provjeri(trenutni_cvor.djeca[1], trenutni_cvor_u_tablici_znakova)    #provjeri(<deklaracija>)

            trenutni_cvor.broj_varijabli = trenutni_cvor.djeca[0].broj_varijabli + trenutni_cvor.djeca[0].broj_varijabli


        else:
            ispis_greske(trenutni_cvor)



##################### <deklaracija> ##################### 
    
    elif trenutni_cvor.jedinka.IDN == '<deklaracija>':

        ##<deklaracija> ::= <ime_tipa> <lista_init_deklaratora> TOCKAZAREZ
        if trenutni_cvor.djeca[0].jedinka.IDN == '<ime_tipa>':
            
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<ime_tipa>)
            
            trenutni_cvor.djeca[1].ntip = trenutni_cvor.djeca[0].tip[:]
            provjeri(trenutni_cvor.djeca[1], trenutni_cvor_u_tablici_znakova)    #provjeri(<lista_init_deklaratora>)    

            trenutni_cvor.broj_varijabli = trenutni_cvor.djeca[1].broj_varijabli


        else:
            ispis_greske(trenutni_cvor)



##################### <lista_init_deklaratora> ##################### 
    
    elif trenutni_cvor.jedinka.IDN == '<lista_init_deklaratora>':
        ##<lista_init_deklaratora> ::= <init_deklarator>
        if trenutni_cvor.djeca[0].jedinka.IDN == '<init_deklarator>':
            
            trenutni_cvor.djeca[0].ntip = trenutni_cvor.ntip[:]
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<init_deklarator>)
            trenutni_cvor.broj_varijabli = 1


        ##<lista_init_deklaratora> ::= <lista_init_deklaratora> ZAREZ <init_deklarator>
        elif trenutni_cvor.djeca[0].jedinka.IDN == '<lista_init_deklaratora>':
            
            trenutni_cvor.djeca[0].ntip = trenutni_cvor.ntip[:]
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<lista_init_deklaratora>)
            
            trenutni_cvor.djeca[2].ntip = trenutni_cvor.ntip[:]
            provjeri(trenutni_cvor.djeca[2], trenutni_cvor_u_tablici_znakova)    #provjeri(<init_deklarator>)

            trenutni_cvor.broj_varijabli = trenutni_cvor.djeca[0].broj_varijabli + 1


        else:
            ispis_greske(trenutni_cvor)



##################### <init_deklarator> ##################### 
    
    elif trenutni_cvor.jedinka.IDN == '<init_deklarator>':

        ##<init_deklarator> ::= <izravni_deklarator>
        if len(trenutni_cvor.djeca) == 1:
            
            trenutni_cvor.djeca[0].ntip = trenutni_cvor.ntip[:]
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<izravni_deklarator>)

            if trenutni_cvor.djeca[0].tip[0] in ['const_int', 'const_char']:
                ispis_greske(trenutni_cvor)

            if trenutni_cvor.djeca[0].tip[0] == 'niz' and trenutni_cvor.djeca[0].tip[1] in ['const_int','const_char']:
                ispis_greske(trenutni_cvor)

            trenutni_cvor.broj_varijabli = trenutni_cvor.djeca[0].broj_varijabli

            ##ako se varijabla nalazi u globalnom djelokrugu
            if trenutni_cvor_u_tablici_znakova.trenutno_u_djelokrugu == None:
                globalne_varijable[trenutni_cvor.djeca[0].djeca[0].jedinka.ime] = 0



        ##<init_deklarator> ::= <izravni_deklarator> OP_PRIDRUZI <inicijalizator>
        elif len(trenutni_cvor.djeca) == 3:
            
            trenutni_cvor.djeca[0].ntip = trenutni_cvor.ntip[:]
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<izravni_deklarator>)

            provjeri(trenutni_cvor.djeca[2], trenutni_cvor_u_tablici_znakova)    #provjeri(<inicijalizator>)



            IDN = provjeri_trenutni_cvor_znakova(trenutni_cvor.djeca[0].djeca[0].jedinka, trenutni_cvor_u_tablici_znakova)
            IDN.vrijednost = trenutni_cvor.djeca[2].vrijednost


            if trenutni_cvor.djeca[0].tip[0] in ['int','char','const_int','const_char']:

                if provjeri_tip(trenutni_cvor.djeca[2].tip, trenutni_cvor.djeca[0].tip) == False:
                    ispis_greske(trenutni_cvor)


            elif trenutni_cvor.djeca[0].tip[0] == 'niz':
                if trenutni_cvor.djeca[0].tip[1] in ['int','char','const_int','const_char']:
                    if trenutni_cvor.djeca[2].br_elem > trenutni_cvor.djeca[0].br_elem:
                        ispis_greske(trenutni_cvor)

                    for tip in trenutni_cvor.djeca[2].tip[1:]:
                        if provjeri_tip([tip], [trenutni_cvor.djeca[0].tip[1]]) == False:
                            ispis_greske(trenutni_cvor)
                            
                else:
                    ispis_greske(trenutni_cvor)
            else:
                ispis_greske(trenutni_cvor)


            trenutni_cvor.broj_varijabli = trenutni_cvor.djeca[0].broj_varijabli

            ##ako se varijabla nalazi u globalnom djelokrugu
            if trenutni_cvor_u_tablici_znakova.trenutno_u_djelokrugu == None:
                globalne_varijable[trenutni_cvor.djeca[0].djeca[0].jedinka.ime] = trenutni_cvor.djeca[2].vrijednost


        else:
            ispis_greske(trenutni_cvor)



##################### <izravni_deklarator> ##################### 
    
    elif trenutni_cvor.jedinka.IDN == '<izravni_deklarator>':

        ##<izravni_deklarator> ::= IDN
        if len(trenutni_cvor.djeca) == 1:
            
            if 'void' in trenutni_cvor.djeca[0].ntip:
                ispis_greske(trenutni_cvor)

            IDN = provjeri_trenutni_cvor_znakova(trenutni_cvor.djeca[0].jedinka, trenutni_cvor_u_tablici_znakova)
        
            if IDN:
                ispis_greske(trenutni_cvor)

            ##zabiljezi deklaraciju IDN.ime s odgovarajucim tipom
            IDN = identifikator(trenutni_cvor.ntip[:], trenutni_cvor.djeca[0].jedinka.ime, 1, 1)
            trenutni_cvor_u_tablici_znakova.dodaj_IDN(IDN)

            trenutni_cvor.tip = trenutni_cvor.ntip[:]



        ##<izravni_deklarator> ::= IDN L_UGL_ZAGRADA BROJ D_UGL_ZAGRADA
        elif trenutni_cvor.djeca[2].jedinka.IDN == 'BROJ':
            
            if 'void' in trenutni_cvor.ntip:
                ispis_greske(trenutni_cvor)

            
            IDN = provjeri_trenutni_cvor_znakova(trenutni_cvor.djeca[0].jedinka, trenutni_cvor_u_tablici_znakova)
            
            ##provjeri da IDN.ime nije vec definirano
            if IDN:
                ispis_greske(trenutni_cvor)


            if int(trenutni_cvor.djeca[2].jedinka.ime) < 0 or int(trenutni_cvor.djeca[2].jedinka.ime) > 1024:
                ispis_greske(trenutni_cvor)


            ##zabiljezi deklaraciju IDN.ime s odgovarajucim tipom
            
            IDN = identifikator(['niz',trenutni_cvor.ntip[0]], trenutni_cvor.djeca[0].jedinka.ime, 1, 1)
            trenutni_cvor_u_tablici_znakova.dodaj_IDN(IDN)

            trenutni_cvor.tip = ['niz', trenutni_cvor.ntip[0]]
            trenutni_cvor.br_elem = int(trenutni_cvor.djeca[2].jedinka.ime)


        ##<izravni_deklarator> ::= IDN L_ZAGRADA KR_VOID D_ZAGRADA
        elif trenutni_cvor.djeca[2].jedinka.IDN == 'KR_VOID':

            #provjeri je li IDN.ime deklarirano u lok. djelokrugu
            IDN = provjeri_trenutni_cvor_znakova(trenutni_cvor.djeca[0].jedinka, trenutni_cvor_u_tablici_znakova)

            if IDN != None:

                #funkcija je deklarirana s istim tipom
                if IDN.tip[1] != trenutni_cvor.ntip:
                    ispis_greske(trenutni_cvor)

                #funkcija ne prima parametre (VOID)
                if 'void' not in IDN.tip[2:]:
                    ispis_greske(trenutni_cvor)

            else:
                ##zabiljezi delaraciju IDN.ime s odgovarajucim tipom ako ista funkcija vec nije deklarirana u lok. djelokrugu
                IDN = identifikator(['fun', trenutni_cvor.ntip[0], 'void'], trenutni_cvor.djeca[0].jedinka.ime, 0, 0)
                trenutni_cvor_u_tablici_znakova.dodaj_IDN(IDN)                      


            trenutni_cvor.tip = ['fun', trenutni_cvor.ntip[0], 'void']


        ##<izravni_deklarator> ::= IDN L_ZAGRADA <lista_parametara> D_ZAGRADA
        elif trenutni_cvor.djeca[2].jedinka.IDN == '<lista_parametara>':
            
            provjeri(trenutni_cvor.djeca[2], trenutni_cvor_u_tablici_znakova)    #provjeri(<lista_parametara>)

            #provjeri je li IDN.ime deklarirano u lok. djelokrugu
            IDN = provjeri_trenutni_cvor_znakova(trenutni_cvor.djeca[0].jedinka, trenutni_cvor_u_tablici_znakova)

            if IDN != None:

                #funkcija je deklarirana s istim tipom
                if IDN.tip[1] != trenutni_cvor.ntip[0]:
                    ispis_greske(trenutni_cvor)

                #funkcija prima iste parametre koji su vec deklarirani
                if IDN.tip[2:] != trenutni_cvor.djeca[2].tip:
                    ispis_greske(trenutni_cvor)

            else:
                ##zabiljezi deklaraciju IDN.ime s odgovarajucim tipom ako ista funkcija vec nije deklarirana u lok. djelokrugu
                tip_funkcije = ['fun', trenutni_cvor.ntip[0]]       #oznaci tip funkcije
                tip_funkcije.extend(trenutni_cvor.djeca[2].tip)     #dodaj listu tipova parametara

                IDN = identifikator(tip_funkcije, trenutni_cvor.djeca[0].jedinka.ime, 0, 0)
                trenutni_cvor_u_tablici_znakova.dodaj_IDN(IDN)
            

            trenutni_cvor.tip = ['fun', trenutni_cvor.ntip[0]]
            trenutni_cvor.tip.extend(trenutni_cvor.djeca[2].tip)


        else:
            ispis_greske(trenutni_cvor)


##################### <inicijalizator> ##################### 
    
    elif trenutni_cvor.jedinka.IDN == '<inicijalizator>':

        ##<inicijalizator> ::= <izraz_pridruzivanja>
        if trenutni_cvor.djeca[0].jedinka.IDN == '<izraz_pridruzivanja>':
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<izraz_pridruzivanja>)
            
            cvor_NIZ_ZNAKOVA = None
            treba_provjeriti = trenutni_cvor.djeca[:]

            ##ako <izraz_pridruzivanja> => NIZ_ZNAKOVA (nizom produkcija)
            while len(treba_provjeriti) > 0:

                if treba_provjeriti[0].jedinka.IDN == 'NIZ_ZNAKOVA':
                    cvor_NIZ_ZNAKOVA = treba_provjeriti[0]
                    break

                else:
                    if len(treba_provjeriti[0].djeca) > 0:
                        treba_provjeriti.extend(treba_provjeriti[0].djeca)

                del treba_provjeriti[0]

            if cvor_NIZ_ZNAKOVA != None:
                trenutni_cvor.br_elem = len(cvor_NIZ_ZNAKOVA.jedinka.ime) - 2 + 1   #-2 zbog "", +1 zbog \0
                trenutni_cvor.tip = []      ###################TU SI MAKNUO ['niz']!!!! ######################
                for i in range(trenutni_cvor.br_elem):
                    trenutni_cvor.tip.append('char')

            else:
                trenutni_cvor.tip = trenutni_cvor.djeca[0].tip[:]


            trenutni_cvor.vrijednost = trenutni_cvor.djeca[0].vrijednost
            


        ##<inicijalizator> ::= L_VIT_ZAGRADA <lista_izraza_pridruzivanja> D_VIT_ZAGRADA
        elif trenutni_cvor.djeca[0].jedinka.IDN == 'L_VIT_ZAGRADA':
            provjeri(trenutni_cvor.djeca[1], trenutni_cvor_u_tablici_znakova)    #provjeri(<lista_izraza_pridruzivanja>)
            trenutni_cvor.br_elem = trenutni_cvor.djeca[1].br_elem
            trenutni_cvor.tip = trenutni_cvor.djeca[1].tip[:]

        else:
            ispis_greske(trenutni_cvor)

            
##################### <lista_izraza_pridruzivanja> ##################### 
    
    elif trenutni_cvor.jedinka.IDN == '<lista_izraza_pridruzivanja>':

        ##<lista_izraza_pridruzivanja> ::= <izraz_pridruzivanja>
        if trenutni_cvor.djeca[0].jedinka.IDN == '<izraz_pridruzivanja>':
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<izraz_pridruzivanja>)
            trenutni_cvor.tip = trenutni_cvor.djeca[0].tip[:]
            trenutni_cvor.br_elem = 1

        ##<lista_izraza_pridruzivanja> ::= <lista_izraza_pridruzivanja> ZAREZ <izraz_pridruzivanja>
        elif trenutni_cvor.djeca[0].jedinka.IDN == '<lista_izraza_pridruzivanja>':
            provjeri(trenutni_cvor.djeca[0], trenutni_cvor_u_tablici_znakova)    #provjeri(<lista_izraza_pridruzivanja>)
            provjeri(trenutni_cvor.djeca[2], trenutni_cvor_u_tablici_znakova)    #provjeri(<izraz_pridruzivanja>)

            trenutni_cvor.tip = trenutni_cvor.djeca[0].tip + trenutni_cvor.djeca[2].tip
            trenutni_cvor.br_elem = trenutni_cvor.djeca[0].br_elem + 1

        else:
            ispis_greske(trenutni_cvor)

    else:
        ispis_greske(trenutni_cvor)




################# GLAVNI PROGRAM #################

outputFile = open("./a.frisc", "w")     #file u koji se upisuje generirani kod

ulaz = sys.stdin.readlines()



razina = 0
dict_razina = {}

jedinka = leksJedinka(ulaz[0].rstrip())
korijen = cvor(jedinka)
dict_razina[0] = korijen


## izgradnja stabla od ulazne datoteke
for red in ulaz[1:]:
    broj_razmaka = len(red) - len(red.lstrip())
    red = red.lstrip().rstrip()
    
    if red[0] == '<':
        jedinka = leksJedinka(red.rstrip())

    else:
        red = red.split(' ')
        jedinka = leksJedinka(red[0], red[1], (' ').join(red[2:]))

    Cvor = cvor(jedinka)

    if broj_razmaka > razina:
        razina = broj_razmaka

    elif broj_razmaka < razina:
        razina = broj_razmaka

    dict_razina[razina] = Cvor

    dict_razina[razina-1].dodajDijete(Cvor)



korijen_znakova = cvorZnak()


outputFile.write("\tMOVE 40000,R7\n\tCALL F_MAIN\n\tHALT\n\n")
outputFile.flush()

globalne_varijable = {}
broj_konstante = 0
u_petlji = 0
odmak = {}
dodatni_odmak = 0

provjeri(korijen, korijen_znakova)


for varijabla in globalne_varijable:
    outputFile.write("\nG_%s\tDW %%D %d" % (varijabla.upper(), globalne_varijable[varijabla]))


outputFile.write("\n\n")
outputFile.close()

