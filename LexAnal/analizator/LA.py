#!/usr/bin/python
import sys
sys.path.append("../")
from automat import *
from ParStanja import *
from pravilo import *
from leksJedinka import *
import pickle

inputFile = open( "data.p", "rb" )

stanje = pickle.load( inputFile )

listaPravila = pickle.load( inputFile )

kod = sys.stdin.read()

redak = 1
leksJedinke = []

while len(kod) > 0:
        #print( kod[:5])
        indexPravila = -1
        maxDuljina = 0
        povecanjeRetka = False

        for pravilo in listaPravila:
                if pravilo.trenutno_stanje == stanje:
                        duljinaNiza = pravilo.automat.najdulji_podniz(kod)
                        if duljinaNiza > maxDuljina:
                                indexPravila = listaPravila.index(pravilo)
                                maxDuljina = duljinaNiza
        
        if indexPravila == -1:
                sys.stderr.write( "Syntax error at line:", redak, "key:", kod[:1])
                if kod[0] == '\n':
                        redak += 1
                kod = kod[1:]
                continue

        akcije = listaPravila[indexPravila].naredbe

        for akcija in akcije:
                if akcije.index(akcija) == 0:
                        continue

                if akcija.rstrip() == "NOVI_REDAK":
                        povecanjeRetka = True

                if akcija.split(' ')[0] == "UDJI_U_STANJE":
                        stanje = akcija.split(' ')[1]

                if akcija.split(' ')[0] == "VRATI_SE":
                        maxDuljina = int(akcija.split(' ')[1])
        
        if akcije[0] != '-':
                #print( "----------", stanje, "------------" )
                #print( "----------", akcije)

                jedinka = leksJedinka( akcije[0], redak, kod[:maxDuljina]) 
                leksJedinke.append(jedinka)
                jedinka.ispis()

        if povecanjeRetka:
                redak += 1

        kod = kod[maxDuljina:]

        #print( stanje, " ", kod[:10])