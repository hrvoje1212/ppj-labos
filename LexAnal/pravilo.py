#!/usr/bin/python
import sys
from automat import *

class pravilo:
	def __init__(self, trenutno_stanje = None, automat = automat(), naredbe = []):
		self.trenutno_stanje = trenutno_stanje
		self.automat = automat
		self.naredbe = naredbe
	
	def ispis(self):
		print( self.trenutno_stanje, " -> ", self.automat.regex )
		self.automat.ispis()
		print( "Akcije: ", self.naredbe )
		print( "---------------------" )

