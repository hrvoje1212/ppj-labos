#!/usr/bin/python
from automat import *
from ParStanja import *
from pravilo import *
import pickle
import sys

def je_operator ( izraz, i ):                   #       paran (True) ili neparan (False) broj '\'
    br = 0
    while i - 1 >= 0 and izraz[i-1] == '\\':
        br += 1
        i -= 1
    return br % 2 == 0

def odgovarajuca_zagrada(izraz, i):             #       vraca poziciju odgovarajuce zagrade
    zagrade = 1
    j = i
    for index in range(i + 1, len(izraz)):
        if izraz[index] == '(':
            zagrade += 1
        elif izraz[index] == ')':
            zagrade -= 1
        j += 1
        if zagrade == 0:
            break
    return j

def pretvori ( izraz, automat ):                #       isto kao u pseudokodu
	#print("izraz: ", izraz)
	i = 0
	izbori = []                                 #       tu dodajemo grupirane znakove
	br_zagrada = 0                              #       brojimo otvorene zagrada ispred operatora '|'
	pocetak_negrupiran = 0                      #       index prvog negrupiranog znaka
	for znak in izraz:
		if znak == '('  and je_operator( izraz, i ) :
			br_zagrada += 1

		elif znak == ')' and je_operator( izraz, i ) :
			br_zagrada -= 1

		elif br_zagrada == 0 and znak == '|' and je_operator( izraz, i ) :      #       grupiranje
			izbori.append( izraz[pocetak_negrupiran : i] )
			pocetak_negrupiran = i + 1
		i += 1
	
	if i == len(izraz):
		izbori.append( izraz[pocetak_negrupiran : ] )

	lijevo_stanje = automat.novo_stanje()
	desno_stanje = automat.novo_stanje()
	
	#print("izbor", izbori )

	if len(izbori) != 1:                              #       barem jedan operator izbora
		for i in range(len(izbori)):
			privremeno = pretvori(izbori[i], automat)
			automat.dodaj_epsilon_prijelaz(lijevo_stanje, privremeno.lijevo_stanje)
			automat.dodaj_epsilon_prijelaz(privremeno.desno_stanje, desno_stanje)
	else:
		prefiksirano = False
		zadnje_stanje = lijevo_stanje
		
		i = 0
		while i < len(izbori[0]):
			#   slucaj 1 prefiksirano
			trenutni_znak = izbori[0][i]
			if prefiksirano:
				#print( "    prefiksiran ---" )
				prefiksirano = False

				if trenutni_znak == 't':
					prijelazni_znak = '\t'
				elif trenutni_znak == 'n':
					prijelazni_znak = '\n'
				elif trenutni_znak == '_':
					prijelazni_znak = ' '
				else:
					prijelazni_znak = trenutni_znak

				#print( "-------- ", trenutni_znak, " pozicija ", i)
				a = automat.novo_stanje()
				b = automat.novo_stanje()
				automat.dodaj_prijelaz(a, b, prijelazni_znak)

			#   slucaj 2 ne prefiksirano
			else:
				#print( " NE prefiksiran ---" )
				if trenutni_znak == '\\':
					prefiksirano = True
					i += 1
					continue

				if trenutni_znak != '(':
					#print( "-------- ", trenutni_znak, " pozicija ", i)
					a = automat.novo_stanje()
					b = automat.novo_stanje()
					if trenutni_znak == '$':
						automat.dodaj_epsilon_prijelaz(a, b)
					else:
						automat.dodaj_prijelaz(a, b, trenutni_znak)
				else:
					j = odgovarajuca_zagrada(izbori[0], i)
					#print("zagrada1 ------- ", izraz[i+1 : j], "  ", i, j)
					privremeno = pretvori(izraz[i+1 : j], automat)
					a = privremeno.lijevo_stanje
					b = privremeno.desno_stanje
					i = j
					#print("zagrada2 ------- ", izraz[i+1 : j], "  ", i, j)

			#   provjera ponavljanja
			if i + 1 < len(izbori[0]) and izbori[0][i+1] == '*':
				#print( "--- kleenov --- ", izbori[0], " ", i)
				#print( "--- izraz --- ", izraz )
				x = a
				y = b
				a = automat.novo_stanje()
				b = automat.novo_stanje()
				automat.dodaj_epsilon_prijelaz(a, x)
				automat.dodaj_epsilon_prijelaz(y, b)
				automat.dodaj_epsilon_prijelaz(a, b)
				automat.dodaj_epsilon_prijelaz(y, x)
				i += 1

			#   povezivanje s prethodnim podizrazom
			automat.dodaj_epsilon_prijelaz(zadnje_stanje, a)
			zadnje_stanje = b

			i += 1

		automat.dodaj_epsilon_prijelaz(zadnje_stanje, desno_stanje)

	return ParStanja(lijevo_stanje, desno_stanje)



def zamijeni(regIzrazi, izraz):

	pronadjeni_izrazi = []

	for i in range( len(izraz) ):
		if izraz[i] == '{' and je_operator(izraz, i):
			pocetak = i
		elif izraz[i] == '}' and je_operator(izraz, i):
			kraj = i
			pronadjeni_izrazi.append( izraz[pocetak : kraj+1] )
	
	for i in pronadjeni_izrazi:
		izraz = izraz.replace( i, '(' + regIzrazi[i] + ')' )
	
	return izraz


ulaz = sys.stdin.readlines()

#f = open("./testovi/ppjC.txt","r")
#ulaz = f.readlines()

regIzrazi = {}                                  #       rjecnik regularnih izraza oblika

outputFile = open("./analizator/data.p", "wb")
citaIzraze = True
citaNaredbe = False
listaPravila = []

for red in ulaz:
	if red[:2] == '%X':
		citaIzraze = False
		stanja = red.rstrip().split(' ')
		stanja = stanja[1:]
		pickle.dump( stanja[0], outputFile )
		continue

	if red[:2] == '%L':
		leksickeJedinke = red.rstrip().split(' ')
		leksickeJedinke = leksickeJedinke[1:]
		continue
	
	if citaIzraze:
		red = red.rstrip().split(' ')
		kljuc = red[0]
		izraz = red[1]
		regIzrazi[kljuc] = zamijeni( regIzrazi, izraz )
	else:
		if red[0] == '<':
			trenutno_stanje = red[1 : red.index('>')]
			regex = zamijeni( regIzrazi, red[red.index('>')+1 :].rstrip() )

		if red[0] == '{':
			citaNaredbe = True
			naredbe = []
			continue

		if red[0] == '}':
			citaNaredbe = False
			automata = automat()
			automata.regex = regex
			pretvori( regex, automata )
			listaPravila.append( pravilo(trenutno_stanje, automata, naredbe) )

		if citaNaredbe:
			naredbe.append( red.rstrip() )

#for i in listaPravila:
#	i.ispis()

pickle.dump(listaPravila, outputFile)
outputFile.close()
