#!/usr/bin/python

class automat:
	def __init__(self):
		self.brojStanja = 0
		self.prijelazi = {}
		self.regex = ""

	def novo_stanje(self):
		self.brojStanja += 1
		return self.brojStanja - 1
	
	def dodaj_epsilon_prijelaz(self, stanje_iz, stanje_u):
		self.dodaj_prijelaz(stanje_iz, stanje_u, '$$')

	def dodaj_prijelaz(self, stanje_iz, stanje_u, prijelazni_znak):
		if stanje_iz not in self.prijelazi.keys():
			self.prijelazi[stanje_iz] = {}
		if prijelazni_znak not in self.prijelazi[stanje_iz].keys():
			self.prijelazi[stanje_iz][prijelazni_znak] = []
		self.prijelazi[stanje_iz][prijelazni_znak].append(stanje_u)

	def ispis(self):
		for iz in self.prijelazi.keys():
			for znak in self.prijelazi[iz]:
				print(iz, " : ", znak, " -> " , self.prijelazi[iz][znak])

	def e_okruzenje(self, stanje):
		okruzenje = [stanje]
		i = 0
		while True:
			try:
				if '$$' in self.prijelazi[okruzenje[i]].keys():
					pom = self.prijelazi[okruzenje[i]]['$$']
					for prijelaz in pom:
						if prijelaz not in okruzenje:
							okruzenje.append(prijelaz)
			except KeyError:
				pass
			if okruzenje[i] == okruzenje[-1]:
				break
			i += 1
		return okruzenje

	def najdulji_podniz(self, niz):
		najniz = ""
		q = self.e_okruzenje(0)
		for index in range(len(niz)):
			s = []
			for stanje in q:
				try:
					if niz[index] in self.prijelazi[stanje].keys():
						pom = self.prijelazi[stanje][niz[index]]
						for i in pom:
							for j in self.e_okruzenje(i):
								if j not in s:
									s.append(j)
				except KeyError:
					pass
			if 1 in s:
				najniz = niz[:index+1]

			if len(s) == 0:
				break
			q = s[:]

		return len(najniz)
