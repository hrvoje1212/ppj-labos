#!/usr/bin/python2.7
import sys
from leksJedinka import *
from node import *
sys.path.append("../")
import pickle


def provjeriUzorak(stack, produkcija):
	odgovara = True
	t = stack[:-1]
	t = t[::-2]
	uzorak = produkcija[::-1]
	
	for i in range(len(uzorak)):
		if uzorak[i] != t[i].jedinka.IDN:
			odgovara = False
			break

	return odgovara

def simuliraj(stack, jedinke, akcija, novoStanje, sinc):
		
	i = 0
	
	while i < len(jedinke):
		#print "========================== stack", jedinke[i].IDN, i
		#for st in stack[1::2]:
			#print "[%s]" %(st)
		
		trenutnoStanje = stack[-1]
		
		#print "========================== STANJE", trenutnoStanje

		znak = jedinke[i].IDN

		if znak not in akcija[trenutnoStanje].keys():
			#print "ODBACI 1 !!!!!!!!!!!", i
			#print >> sys.stderr, "%s: Error!\nExpected: %s\nGot: %s %s" % (jedinke[i].red, akcija[trenutnoStanje].keys(), jedinke[i].IDN, jedinke[i].ime)

			while jedinke[i].IDN not in sinc:
				#print jedinke[i].IDN

				i += 1
		
			while stack[-1] not in akcija.keys() or jedinke[i].IDN not in akcija[stack[-1]].keys():
				stack = stack[:-2]

			#print i
			continue
	
		listaAkcije = akcija[trenutnoStanje][znak]

		if listaAkcije[0] == "POMAKNI":
			#print "POMAKNI"
			newNode = node(jedinke[i])
			stack.append(newNode)
			stack.append(listaAkcije[1])
			i += 1
			continue

		if listaAkcije[0] == "REDUCIRAJ":
			lijevaStrana = listaAkcije[1].split("->")[0]
			desnaStrana = listaAkcije[1].split("->")[1].split(',')
			#print "REDUCIRAJ", listaAkcije[1]
			
			if desnaStrana[0] == '$':
				newNode = node(leksJedinka(lijevaStrana))
				newNode.djeca.append(node(leksJedinka("$")))
				stack.append(newNode)
				stack.append(novoStanje[stack[-2]][newNode.jedinka.IDN])
				continue

			
			if provjeriUzorak(stack, desnaStrana) == False:
				#print >> sys.stderr, "%s: Error!\nExpected: %s\nGot: %s %s" % (jedinke[i].red, akcija[trenutnoStanje].keys(), jedinke[i].IDN, jedinke[i].ime)

				while jedinke[i].IDN not in sinc:
					i += 1
		
				while stack[-1] not in akcija.keys() or jedinke[i].IDN not in akcija[stack[-1]].keys():
					stack = stack[:-2]

				continue
			
			uzorak = stack[-len(desnaStrana)*2:]
			stack = stack[:-len(desnaStrana)*2]

			uzorak = uzorak[:-1]
			uzorak = uzorak[::-2]
			novaDjeca = []
			#print "DJECA____________:"
			

			for element in uzorak:
				novaDjeca.append(element)
				#print element

			newNode = node(leksJedinka(lijevaStrana))
			newNode.djeca = novaDjeca
			#newNode.ispisStabla("")

			stack.append(newNode)
			if newNode.jedinka.IDN not in novoStanje[stack[-2]].keys():
				odbaci(stack, i)
				continue
			stack.append(novoStanje[stack[-2]][newNode.jedinka.IDN])

			continue

		if listaAkcije[0] == "PRIHVATI":
			#print "PRIHVATI"
			#print "========================== stack", jedinke[i].IDN, i
			#for st in stack:
				#print "[%s]" %(st)
			return stack[-2]
	
	
	
	return None

inputFile = open("data1.p", "rb")

pocetno_stanje = pickle.load(inputFile)
sinkronizacijski_znakovi = pickle.load(inputFile)
akcija = pickle.load(inputFile)
novo_stanje = pickle.load(inputFile)
jedinke = []
stack = []
stack.append(pocetno_stanje)

ulaz = sys.stdin.readlines()

#f = open ("../testovi/lab2_ispitni_primjeri/12ppjC/test.in", "r")
#ulaz = f.readlines()


for red in ulaz:
	parseJedinke = red.rstrip().split(" ")
	jedinke.append(leksJedinka(parseJedinke[0], parseJedinke[1], (' ').join(parseJedinke[2:])))

jedinke.append(leksJedinka("#"))
sinkronizacijski_znakovi.append("#")

root = simuliraj(stack, jedinke, akcija, novo_stanje, sinkronizacijski_znakovi)
root.ispisStabla("")