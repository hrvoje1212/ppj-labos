#!/usr/bin/python2.7
from leksJedinka import *

class node:
	def __init__(self, jedinka):
		self.jedinka = jedinka
		self.djeca = []

	def dodajDijete(self, dijete):
		self.djeca.append(dijete)

	def __str__(self):
		if self.jedinka.IDN[0] == '<' or self.jedinka.IDN == '$':
			return self.jedinka.IDN
		else:
			return "%s %s %s" % (self.jedinka.IDN, self.jedinka.red, self.jedinka.ime)
		
	def ispisStabla(self, razmak):
		print "%s%s" % (razmak, self)
		for dijete in reversed(self.djeca):
			dijete.ispisStabla(razmak + " ")

